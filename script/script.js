$(document).ready(function(){
    $('.slider').slick({
        arrows:true,
        dots:true,
        slidesToShow:5,
        autoplay:true,
        speed:1000,
        autoplaySpeed:800,
        responsive:[
            {
                breakpoint: 1750,
                settings: {
                    slidesToShow:4
                }
            },
            {
                breakpoint: 1400,
                settings: {
                    slidesToShow:3
                }
            },
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow:2
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow:1
                }
            }
        ]
    });
});

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 8000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
